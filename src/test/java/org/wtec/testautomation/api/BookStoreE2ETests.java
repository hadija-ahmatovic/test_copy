package org.wtec.testautomation.api;

import com.google.gson.Gson;
import io.cucumber.java.Before;
import io.cucumber.testng.CucumberOptions;
import org.apache.http.HttpStatus;
import org.wtec.testautomation.api.clients.AccountClient;
import org.wtec.testautomation.api.clients.BookStoreClient;
import org.wtec.testautomation.api.models.TokenResponse;
import org.wtec.testautomation.api.models.UserAuth;
import org.wtec.testautomation.core.BaseAPITest;

@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber-html-report.html" },
        features = {"src/test/resources/features"},
        tags = "@end2end",
        monochrome = true
)
public class BookStoreE2ETests extends BaseAPITest {
    @Before
    public void setup() {
        UserAuth userAuth = new UserAuth("TestUser1", "Password_1!");

        var tokenResponse = new AccountClient().generateToken(userAuth)
                .assertStatusCode(HttpStatus.SC_OK)
                .getResponse();

        var token = new Gson().fromJson(tokenResponse.asString(), TokenResponse.class).getToken();

        BookStoreClient.setAuthorization("Bearer " + token);
        AccountClient.setAuthorization("Bearer " + token);
    }
}
