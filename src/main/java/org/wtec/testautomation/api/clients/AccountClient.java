package org.wtec.testautomation.api.clients;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpHeaders;
import org.wtec.testautomation.api.models.Book;
import org.wtec.testautomation.api.models.User;
import org.wtec.testautomation.api.models.UserAuth;
import org.wtec.testautomation.core.api.BaseAPIClient;

public class AccountClient extends BaseAPIClient<AccountClient> {
    private final String ENDPOINT = "/Account/v1";

    @Getter
    @Setter
    private static String authorization;

    public AccountClient authorize(UserAuth userAuth) {
        setResponse(RestAssured.given()
                .contentType(ContentType.JSON)
                .body(userAuth)
                .when()
                .post(ENDPOINT + "/Authorized"));
        return this;
    }

    public AccountClient generateToken(UserAuth userAuth) {
        setResponse(RestAssured.given()
                .contentType(ContentType.JSON)
                .body(userAuth)
                .when()
                .post(ENDPOINT + "/GenerateToken"));
        return this;
    }

    public AccountClient registerUser(UserAuth userAuth) {
        setResponse(RestAssured.given()
                .contentType(ContentType.JSON)
                .body(userAuth)
                .when()
                .post(ENDPOINT + "/User"));
        return this;
    }

    public AccountClient getUser(String id) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .when()
                .get(ENDPOINT + "/User/" + id));
        return this;
    }

    public Book getUserBook(String userId, String isbn) {
        var response = getUser(userId)
                .getResponse();

        var user = new Gson().fromJson(response.asString(), User.class);

        return user.getBooks()
                .stream()
                .filter(book -> book.getIsbn().equals(isbn))
                .findFirst()
                .orElse(null);
    }

    public AccountClient deleteUser(String id) {
        setResponse(RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, authorization)
                .when()
                .delete(ENDPOINT + "/User/" + id));
        return this;
    }
}
