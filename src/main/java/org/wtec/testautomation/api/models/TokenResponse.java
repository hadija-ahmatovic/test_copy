package org.wtec.testautomation.api.models;

import lombok.Data;

import java.util.Date;

@Data
public class TokenResponse {
    private final String token;
    private final Date expires;
    private final String status;
    private final String result;
}
