package org.wtec.testautomation.api.models;

import lombok.Data;

import java.util.List;

@Data
public class User {
    private final String userId;
    private final String username;
    private final List<Book> books;

}
